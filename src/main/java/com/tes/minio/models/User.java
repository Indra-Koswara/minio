package com.tes.minio.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "user", schema = "public")
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class User implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_user_user_id_seq")
	@SequenceGenerator(name = "generator_user_user_id_seq", sequenceName = "user_user_id_seq", schema = "public", allocationSize = 1)
	@Column(name = "user_id", unique = true, nullable = false)
	private long userId;
	@Column(name = "name")
	private String name;
	@Column(name ="image")
	private String image;
}
