package com.tes.minio.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.core.enums.ActionFlow;
import com.io.iona.implementations.pagination.DefaultPagingParameter;
import com.io.iona.springboot.actionflows.custom.CustomAfterReadAll;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;
import com.tes.minio.dtos.UserDTO;
import com.tes.minio.models.User;
import com.tes.minio.setting.MinIO;

@RestController
@RequestMapping("user")
public class UserController extends HibernateCRUDController<User, UserDTO> implements CustomAfterReadAll<User, UserDTO>{

	@Autowired
	MinIO minio;
	
	
	@Override
	public List<UserDTO> afterReadAll(HibernateDataUtility arg0, HibernateDataSource<User, UserDTO> dataSource,
			DefaultPagingParameter arg2) throws Exception {
		List<User> listUser = dataSource.getResult(ActionFlow.ON_READ_ALL_ITEMS, List.class);
		List<UserDTO> result = new ArrayList<UserDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for (User user : listUser) {
			UserDTO temp = modelMapper.map(user, UserDTO.class);
			String fileName = "picture/" +temp.getImage();
			String bucketName = "pegadaian-foto";
			String link = minio.minIO().presignedGetObject(bucketName, fileName);
			temp.setImage(link);
			result.add(temp);
		}
		return result;
	}

}
