package com.tes.minio.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tes.minio.setting.MinIO;

import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.XmlParserException;

@RestController
@RequestMapping("minIo")
public class minioController extends MinIO{

	@GetMapping("download")
	public HashMap<String, Object> downloadFile() throws InvalidEndpointException, InvalidPortException,InvalidKeyException{
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			String fileName = "java.png";
			String objectName = "picture/" + fileName;
			String link = minIO().getObjectUrl(bucketName, objectName);
			result.put("Status", HttpStatus.OK);
			result.put("link", link);
		} catch (Exception e) {
			result.put("Status", HttpStatus.BAD_REQUEST);
			result.put("Error message", e.toString());
		}
		return result;
	}
	
	@PostMapping("upload")
	public HashMap<String, Object> uploadFile(@RequestParam (value = "file") MultipartFile file) throws InvalidEndpointException, InvalidPortException{
		HashMap<String, Object> result = new HashMap<String, Object>();
		try {
			String fileName = file.getOriginalFilename().replace(" ", "_");
			String objectName = "picture/" + fileName;
			Path filePath = Files.createTempFile("file", ".jpg");
			file.transferTo(filePath);
			String link = minIO().presignedGetObject(bucketName, objectName);
			result.put("Status", HttpStatus.ACCEPTED);
			result.put("Message", fileName + " upload sukses");
			result.put("Link", link);
		} catch (Exception e) {
			result.put("Status", HttpStatus.BAD_REQUEST);
			result.put("Error message", e.toString());
		}
		return result;
	}
	
	public String responeLink(String fileName) throws InvalidEndpointException, InvalidPortException,InvalidKeyException, ErrorResponseException, IllegalArgumentException, InsufficientDataException, InternalException, InvalidBucketNameException, InvalidResponseException, NoSuchAlgorithmException, XmlParserException, IOException{
		String result = " ";
		String objectName = "picture/" + fileName;
		result = minIO().getObjectUrl(bucketName, objectName);
		return result;
	}
}
