package com.tes.minio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tes.minio.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
