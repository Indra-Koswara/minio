package com.tes.minio.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDTO {

	
	private long userId;
	private String name;
	private String image;
}
