package com.tes.minio.setting;

import org.springframework.beans.factory.annotation.Value;

import io.minio.MinioClient;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;

public class MinIO {

	@Value("${minio.bucketName}")
	protected String bucketName;
	@Value("${minio.username}")
	protected String userName;
	@Value("${minio.password}")
	protected String password;
	@Value("${minio.url}")
	protected String url;
	
	public MinioClient minIO() throws InvalidEndpointException, InvalidPortException{
		return new MinioClient(url, userName, password);
	}
}
